#!/bin/sh

# Launch the controller
python3 -m fastchat.serve.controller \
	 --host 0.0.0.0 --port 21001 \
	 > /tmp/fastChat_CONTROLLER.out \
        2> /tmp/fastChat_CONTROLLER.err & 

# Add a delay to allow the controller to boot up
sleep 2

# Launch the model worker
python3 -m fastchat.serve.model_worker \
      --host 0.0.0.0 --port 39805 \
      --controller-address http://0.0.0.0:21001 \
      --worker-address http://0.0.0.0:39805 \
      --model-path /model \
      --model-name 'vicuna-7B' &

# Add a delay to allow the model worker to finish loading
sleep 50

echo "Start Web UI"

# Run the Gradio web server
python3 /app/gradio_web_server.py \
	--port 12345 \
	--controller-url http://0.0.0.0:21001 

# Keep script running
tail -f /dev/null
