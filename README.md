# LLM

This repo contains the details of running a LLM on TIB premises.

The code relies on the [fastchat framework](https://github.com/lm-sys/FastChat) to serve UI and API components.

## Structure
Each folder contains the Dockerfile and the necessary files to run the LLM.

**Note**: Falcon seems to have some problems running via the fastchat framework.

## More Documentation
For more documentation, please refer to this [GDoc](https://docs.google.com/document/d/1WMpR6M7B76cII8LPlfnnoMurLYW7DT9TMwUc5zUa8MA/edit?usp=sharing)