#!/bin/sh

MODEL_DIR="models--tiiuae--falcon-7b-instruct/snapshots/22225c3ac76bdddc1c6c44ebea0e3109468de29f"

echo "============== Downloading the model and the tokenizer =============="
# Download the model from HF hub
python3 -c 'from transformers import AutoModelForCausalLM, AutoTokenizer; AutoModelForCausalLM.from_pretrained("tiiuae/falcon-7b-instruct", cache_dir="./", trust_remote_code=True); AutoTokenizer.from_pretrained("tiiuae/falcon-7b-instruct", cache_dir="./", trust_remote_code=True)'

echo "============== Starting the controller =============="
# Launch the controller
python3 -m fastchat.serve.controller \
	 --host 0.0.0.0 --port 21001 \
	 > /tmp/fastChat_CONTROLLER.out \
        2> /tmp/fastChat_CONTROLLER.err & 

# Add a delay to allow the controller to boot up
sleep 2

echo "============== Starting the model wroker =============="
# Launch the model worker
python3 /app/model_worker.py \
      --host 0.0.0.0 --port 39805 \
      --controller-address http://0.0.0.0:21001 \
      --worker-address http://0.0.0.0:39805 \
      --model-path "/$MODEL_DIR" \
      --model-name 'Falcon-7B-Instruct' &

# Add a delay to allow the model worker to finish loading
sleep 50

echo "============== Starting the web UI =============="
# Run the Gradio web server
python3 /app/gradio_web_server.py \
	--port 12345 \
	--controller-url http://0.0.0.0:21001 

# Keep script running
tail -f /dev/null
